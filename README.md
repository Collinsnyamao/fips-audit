###### Step 1
Install node

###### Step 2
After installing node

`run npm install`

###### Step 3
run the app using: 

`npm run serve`

ensure to see 

_`fips-audit:server Listening on port 3000 +0ms`_

to confirm it is running.
